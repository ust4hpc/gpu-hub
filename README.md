---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.7.0
  kernelspec:
    display_name: UST4HPC
    language: bash
    name: bash
---

# Déploiement de JupyterHub avec vGPU sur Openstack

## Introduction

Ce dépôt contient le code pour déployer une instance de JupyterHub sur Openstack à l'aide de Kubernetes.
Il est utilisable de trois façons :

> 👶 Utilisation du [notebook UST4HPC](https://ust4hpc-hub.osug.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fust4hpc%2Fgpu-hub&urlpath=lab%2Ftree%2Fgpu-hub%2FREADME.ipynb&branch=master)

> 👸 Utilisation de la VM ust4hpc-jhub.univ-grenoble-alpes.fr 

> 👹 Utilisation de votre propre machine.
> 
> Environnement nécessaire : 
> * un émulateur de terminal et un éditeur de texte (...)
> * La ligne de commande Openstack (pas obligatoire)
> * [Terraform](https://terraform.io)
> * [kubectl](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/) (pas obligatoire)
> * [Helm](https://helm.sh)

La première partie consiste à déployer un cluster Kubernetes sur Openstack. La seconde déploie JupyterHub sur le cluster Kubernetes.

## Terraform

Déploiement d'un cluster Kubernetes sur Openstack : [deploy-k8s](terraform/deploy-k8s.ipynb)

## Helm

Déploiement de JupyterHub sur le cluster Kubernetes : [deploy-z2jh](helm/deploy-z2jh.ipynb)
