module "rke" {
/* 
* Utilisation du module terraform-openstack-rke dans sa version 0.6.2
* La documentation complète du module est disponible sur https://github.com/remche/terraform-openstack-rke
*/
  source              = "remche/rke/openstack"
  version             = "0.6.2"

/*
* Nom du cluster
*/
  cluster_name        = var.cluster_name

/*
* Configuration des DNS pour les réseaux Openstack
*/
  dns_servers         = ["152.77.1.22", "195.83.24.30"]

/*
* Configuration du domaine DNS pour l'ajout automatique des enregistrements
*/
  dns_domain          = "u-ga.fr."

/*
* Utilisation d'une image Ubuntu avec Docker, les drivers Nvidia GRID et le toolkit nvidia-docker
*/
  image_name          = "ubuntu-18.04-docker-vgpu-x86_64"

/*
* Utilisation d'un réseau ouvert depuis l'extérieur pour les floatings IP
*/
  public_net_name     = "dmz"

/* 
* Définition des règles de sécurité. Idéalement on limite l'accès au port 6443 (API K8S)
*/
  secgroup_rules      = [ { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 22 },
                          { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 6443 },
                          { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 80 },
                          { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 443}
                        ]

/*
* Gabarits du master et du worker
* Pour le worker, on choisit un gabarit qui nous permet de réserver un vGPU
*/
  master_flavor_name  = "m1.medium"
  worker_flavor_name  = "V100S-4C.medium"

/*
* Nombre de worker (on en laisse pour les autres !)
*/
  worker_count        = 1

/*
* Classes de stockage pour la persistence des données
*/
  storage_types       = ["solidfire", "ceph"]
  default_storage     = "ceph"
  os_auth_url         = var.os_auth_url
  os_password         = var.os_password

/*
* Nvidia GPU device plugin : https://github.com/NVIDIA/k8s-device-plugin
* Permet d'exposer les ressources GPU via Kubernetes
*/
  addons_include      = ["addons/nvidia-device-plugin.yml"]
}
